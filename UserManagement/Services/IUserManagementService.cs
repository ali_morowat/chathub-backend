﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Models;

namespace UserManagement.Services
{
    public interface IUserManagementService
    {
        Task<IEnumerable<AspNetUsers>> GetAllUsersAsync();
        Task<AspNetUsers> AddUserAsync(AspNetUsers users);
        Task<AspNetUsers> GetByIdAsync(string id);
        Task<AspNetUsers> EditUserAsync(string id, AspNetUsers users);
        Task<AspNetUsers> Remove(string id);
    }
}
