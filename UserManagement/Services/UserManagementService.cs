﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserManagement.Models;

namespace UserManagement.Services
{
    public class UserManagementService : IUserManagementService
    {
        private readonly ChatHubDbContext _context;
        public UserManagementService(ChatHubDbContext context)
        {
            _context = context;
        }

        public async Task<AspNetUsers> AddUserAsync(AspNetUsers users)
        {
            await _context.AspNetUsers.AddAsync(users);
            await _context.SaveChangesAsync();
            return users;
        }

        public async Task<AspNetUsers> EditUserAsync(string id, AspNetUsers users)
        {
            _context.Entry(users).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return users;
        }

        public async Task<IEnumerable<AspNetUsers>> GetAllUsersAsync()
        {
            var users = await _context.AspNetUsers.ToListAsync();
            return users;
        }

        public async Task<AspNetUsers> GetByIdAsync(string id)
        {
            var aspNetUsers  = await _context.AspNetUsers.FindAsync(id);
            if (aspNetUsers == null)
            {
                return null;
            }
            return aspNetUsers;
        }

        public async Task<AspNetUsers> Remove(string id)
        {
            var findUser = await _context.AspNetUsers.FindAsync(id);

            if (findUser != null)
            {
                _context.AspNetUsers.Remove(findUser);
                await _context.SaveChangesAsync();
            }
            else
            {
                return null;
            }
            return findUser;
        }
    }
}
