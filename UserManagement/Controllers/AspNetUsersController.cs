﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UserManagement.Models;
using UserManagement.Services;

namespace UserManagement.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AspNetUsersController : ControllerBase
    {

        private readonly ChatHubDbContext _context;
        private readonly IUserManagementService _userManagementService;

        public AspNetUsersController(ChatHubDbContext context, IUserManagementService userManagementService)
        {
            _userManagementService = userManagementService;
        }

        // GET: api/AspNetUsers
        [HttpGet("Users")]
        public async Task<ActionResult<IEnumerable<AspNetUsers>>> GetAspNetUsers()
        {
            var result =   await _userManagementService.GetAllUsersAsync();
            return Ok(result);
        }

        // GET: api/AspNetUsers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<AspNetUsers>> GetAspNetUsers(string id)
        {
            var aspNetUsers = await _userManagementService.GetByIdAsync(id);
            return aspNetUsers;
        }

        // PUT: api/AspNetUsers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAspNetUsers(string id, AspNetUsers aspNetUsers)
        {
            if (id != aspNetUsers.Id)
            {
                return BadRequest();
            }
            
                await _userManagementService.EditUserAsync(id, aspNetUsers);
         
            return NoContent();
        }

        // POST: api/AspNetUsers
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<IActionResult> PostAspNetUsers(AspNetUsers aspNetUsers)
        {
            try
            {
                var result = await _userManagementService.AddUserAsync(aspNetUsers);
                return Ok(result);
            }
            catch(Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }

        }

        // DELETE: api/AspNetUsers/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<AspNetUsers>> DeleteAspNetUsers(string id)
        {
            await _userManagementService.Remove(id);
            return Ok();
        }
    }
}
