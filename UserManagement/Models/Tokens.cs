﻿using System;
using System.Collections.Generic;

namespace UserManagement.Models
{
    public partial class Tokens
    {
        public int TokenId { get; set; }
        public string UserToken { get; set; }
    }
}
