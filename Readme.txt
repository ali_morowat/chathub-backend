Read me: chat app
This is an app that is created with Angular in the front end and .NetCore (C #) in the back end.
Two services are currently integrated in the backend with an Ocelot API gateway. The services communicate with the front end via a gateway.
I set Ocelot Gateway to port 7000 and ChatHubBackend (service) to port 7001 and ManagementUser (service) to port 7002 in the ocelot.json file in gateway.
The Angular localhost well be running fine on port 4200.

Programms:
Visual Studio for Backend 
Visual Basic for Frontend 

Install Visual Studio
the below packages should be install in Visual Studio:
For the ChatHubBackend Service:

Microsoft.AspNet.SingalR
Microsoft.AspNetCore.App v2.2.0
Microsoft.AspNetCore.Cors
Microsoft.EntityFrameworkCore
Microsoft.AspNetCore.Razor.Design 
Microsoft.EntityFramework.Sqlite
Microsoft.EntityFrameworkCore.SqlServer
Microsoft.EntityFrameworkCore.Tools
Microsoft.NetCoreApp
Microsoft.VisualStudio.Web.CodeGeneration.Design 
Swashbuckle.AspNetCore

For the UserManagement Service:

Microsoft.AspNetCore.Cors
Microsoft.EntityFrameworkCore.Tools
Microsoft.EntityFramework.Sqlite
Microsoft.EntityFrameworkCore.SqlServer
Microsoft.VisualStudio.Web.CodeGeneration.Design 
Swashbuckle.AspNetCore

For Gateway:
Ocelot


Install MSSQL 
Create and Update the Database via Db Migrations command on Visual Studio.
Add manuelly two Rules(Admin & User) by the AspNetRoles Table: 
INSERT INTO YourTableName (Id, Name, NormalizedName)
VALUES ('1', 'Admin', 'Admin');

For UserManagement.integration Test you need to Install via NuGet Package Manager:
FluentAssertions
coverlet.collector
Microsoft.AspNetCore.TestHost
Microsoft.NET.Test.Sdk
Newtonsoft.json
xunit
xunit.runner.VisualStudio
