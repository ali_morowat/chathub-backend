﻿namespace ChatHub_Backend.Model
{
    public class ApplicationSetting
    {
        public string JwtSecret { get; set; }
        public string ClientUrl { get; set; }
    }
}