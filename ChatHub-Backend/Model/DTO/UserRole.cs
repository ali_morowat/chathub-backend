﻿

namespace ChatHub_Backend.Model.DTO
{
    public class UserRole 
    {
         public string RoleId { get; set; } 
         public string RoleName { get; set; } 
    }
}
