﻿namespace ChatHub_Backend.Model.DTO
{
    public class UserStatus
    {
        public string Username { get; set; }
        public bool IsOnline { get; set; }
    }
}