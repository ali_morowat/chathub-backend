﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ChatHub_Backend.Model
{
    public class Token
    {
        [Column] public int TokenId { get; set; }

        [Column] public string UserToken { get; set; }
    }
}