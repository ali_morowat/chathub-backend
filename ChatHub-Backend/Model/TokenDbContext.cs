﻿using Microsoft.EntityFrameworkCore;

namespace ChatHub_Backend.Model
{
    public class TokenDbContext : DbContext
    {
        public TokenDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Token> Tokens { get; set; }
    }
}