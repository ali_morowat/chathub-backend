﻿using System.Linq;
using System.Threading.Tasks;
using ChatHub_Backend.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ChatHub_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserProfileController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UserProfileController(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        //GET: /api/UserProfile
        [HttpGet]
        [Authorize]
        public async Task<object> GetUserProfile()
        {
            var userId = User.Claims.First(c => c.Type == "UserID").Value;
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null) return NotFound();
            return new
            {
                user.UserName
            };
        }


        [HttpGet("ForAdmin")]
        [Authorize(Roles = "Admin")]
        public string GetForAdmin()
        {
            return "web method for admin";
        }

        [HttpGet("ForUser")]
        [Authorize(Roles = "User")]
        public string GetForUser()
        {
            return "web method for user";
        }

        [HttpGet("ForAdminAndUser")]
        [Authorize(Roles = "Admin,User")]
        public string GetForAdminAndUser()
        {
            return "web method for admin";
        }
    }
}