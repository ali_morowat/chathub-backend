﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ChatHub_Backend.Model;
using ChatHub_Backend.Model.DTO;
using ChatHub_Backend.Services;
using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace ChatHub_Backend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ApplicationUserController : ControllerBase
    {
       
        private readonly IChatAuthenticationService _chatAuthenticationService;
       
     
        public ApplicationUserController (IChatAuthenticationService chatAuthenticationService)
        {
            _chatAuthenticationService = chatAuthenticationService;
        }

        /// <summary>
        ///     post the user Information by registration in to server.
        ///     Register : /api/ApplicationUser/Register
        /// </summary>
        /// <param name="informationModel"></param>
        /// <returns></returns>
        [HttpPost("Register")]
        public async Task<IActionResult> UserRegisterAsync(UserInformationModel informationModel)
        {
            try
            {
                var result = await _chatAuthenticationService.RegistrationAsync(informationModel);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        /// <summary>
        ///     Post : /api/ApplicationUser/UserLoginAsync
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> UserLoginAsync(LoginModel model)
        {
            try
            {
                var token = await _chatAuthenticationService.GetAuthenticationTokenForLoginAsync(model);
                return Ok(token);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status401Unauthorized, ex);
            }
        }

        /// <summary>
        ///     Get all Usernames from those User, which are saved in Token table
        /// </summary>
        /// <returns></returns>
        [HttpGet("online")]
        public async Task<ActionResult<IEnumerable<UserInformationModel>>> GetAllOnlineUserAsync()
        {
            try
            {
                var user = await _chatAuthenticationService.GetAllOnlineUserAsync();
                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        [HttpDelete("online/{token}")]
        public IActionResult LogoutUser(string token)
        {
            _chatAuthenticationService.Logout(token);
            return Ok();
        }
      
        [HttpGet("users/status")]
        public async Task<ActionResult<IEnumerable<UserStatus>>> GetUserStatus()
        {
            try
            {
                var user = await _chatAuthenticationService.GetUserStatus();
                return Ok(user);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        [HttpGet("UserRole")]
        public async Task<ActionResult<IEnumerable<UserRole>>> GetAllRoles()
        {
            try
            {
                var role = await _chatAuthenticationService.GetAllRolesAsync();
                return Ok(role);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }

        [HttpGet("UserAssignedRole/{userName}")]
        public async Task<ActionResult<IEnumerable<UserRole>>> GetRoles(string userName)
        {
            try
            {
                var role = await _chatAuthenticationService.GetUserdRole(userName);
                return Ok(role);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex);
            }
        }
    }
}