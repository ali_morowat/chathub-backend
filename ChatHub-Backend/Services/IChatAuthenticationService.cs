﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ChatHub_Backend.Model;
using ChatHub_Backend.Model.DTO;
using Microsoft.AspNetCore.Identity;

namespace ChatHub_Backend.Services
{
    public interface IChatAuthenticationService
    {
       
        Task<IdentityResult> RegistrationAsync(UserInformationModel informationModel);

        Task<string> GetAuthenticationTokenForLoginAsync(LoginModel model);

        //UserInformationModel GetUserNameAccordingToId(int id);
        Task<IEnumerable<string>> GetAllOnlineUserAsync();
        bool Logout(string userToken);
        
        Task<IEnumerable<UserStatus>> GetUserStatus();

        Task<IEnumerable<UserRole>> GetAllRolesAsync();

        Task<IEnumerable<string>> GetUserdRole(string UserName); 


    }
}