﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ChatHub_Backend.Core.ExtensionMethods;
using ChatHub_Backend.Model;
using ChatHub_Backend.Model.DTO;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace ChatHub_Backend.Services
{
    public class ChatAuthenticationService : IChatAuthenticationService
    {
        private readonly ApplicationSetting _appSetting;
        private readonly TokenDbContext _tokenDbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
      


        public ChatAuthenticationService(UserManager<ApplicationUser> userManager,
            
            TokenDbContext tokenDbContext,
            RoleManager<IdentityRole> roleManager,
            IOptions<ApplicationSetting> appSettings)
        {
            _userManager = userManager;
            _appSetting = appSettings.Value;
            _tokenDbContext = tokenDbContext;
            _roleManager = roleManager;
        }


        public async Task<IdentityResult> RegistrationAsync(UserInformationModel informationModel)
        {
            
            // informationModel.Role = SetRoleForUser();
            var applicationUser = new ApplicationUser
            {
                UserName = informationModel.UserName,
                Email = informationModel.Email,
                FullName = informationModel.FullName
            };

            var result = await _userManager.CreateAsync(applicationUser, informationModel.Password);
            await _userManager.AddToRoleAsync(applicationUser, informationModel.IsAdmin ? "Admin" : "User");
            
            return result;
        }

        /// <summary>
        ///     Generate the token with the following Description and storing process
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<string> GetAuthenticationTokenForLoginAsync(LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.UserName);
            

            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                // Get the Role assigned to the user 
                //var role = await _userManager.GetRolesAsync(user);
                //IdentityOptions options = new IdentityOptions();
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new[]
                    {
                        new Claim("UserID", user.Id),
                        new Claim(ClaimTypes.Name, user.UserName),
                        //new Claim(options.ClaimsIdentity.RoleClaimType, role.FirstOrDefault())
                    }),
                    Expires = DateTime.UtcNow.AddSeconds(36000),
                    SigningCredentials = new SigningCredentials(
                        new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_appSetting.JwtSecret))
                        , SecurityAlgorithms.HmacSha256Signature)
                };
                var tokenHandler = new JwtSecurityTokenHandler();
                var securityToken = tokenHandler.CreateToken(tokenDescriptor);
                var token = tokenHandler.WriteToken(securityToken);
                var storeToken = new Token
                {
                    UserToken = token
                };
                _tokenDbContext.Tokens.Add(storeToken);
                _tokenDbContext.SaveChanges();

                return token;
            }

            throw new NotImplementedException("Username or password is incorrect");
        }

        public async Task<IEnumerable<string>> GetUserdRole(string UserName)
        {
            var userName = ""; 

            if(UserName != null)
            {
                userName = UserName;
            } else
            {
                userName = "";
            }
           
            var user = await _userManager.FindByNameAsync(userName);
            var role = await _userManager.GetRolesAsync(user);
            return role;
        }

        /// <summary>
        ///     Get all username of those who have already registered.
        /// </summary>
        /// <returns></returns>
        public async Task<List<string>> GetAllUserName()
        {
            var users = await _userManager.Users.Select(x => x.UserName).ToListAsync();
            return users;
        }

        /// <summary>
        ///     Get all Online user's name
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<string>> GetAllOnlineUserAsync()
        {
            var onlineUsers = new List<string>();

            var storedTokens = await _tokenDbContext.Tokens.Select(x => x.UserToken).ToListAsync();

            foreach (var token in storedTokens)
                try
                {
                    var isCurrentTokenValid = IsTokenValid(token);


                    if (isCurrentTokenValid)
                    {
                        var sToken = new JwtSecurityToken(token);
                        var onlineUser = sToken.Claims.FirstOrDefault(c => c.Type == "unique_name")?.Value;
                        onlineUsers.Add(onlineUser);
                    }
                    // If token was not validated then it means that the token is invalid (probably expired)
                    else
                    {
                        // DELETE token
                        var invalidToken = await _tokenDbContext.Tokens.SingleAsync(x => x.UserToken == token);
                        _tokenDbContext.Remove(invalidToken);
                        await _tokenDbContext.SaveChangesAsync();
                    }
                }
                catch// (Exception ex)
                {
                    //return StatusCode(StatusCodes.Status500InternalServerError, ex);
                }

            return onlineUsers.Distinct().ToList();
        }

        /// <summary>
        ///     Get all username and user status.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<UserStatus>> GetUserStatus()
        {
            var allUsers = await GetAllUserName();
            var onlineUsers = (await GetAllOnlineUserAsync()).ToList();

            var userStatuses = new List<UserStatus>();

            foreach (var user in allUsers)
            {
                var currentUserStatus = new UserStatus
                {
                    Username = user,
                    IsOnline = onlineUsers.Contains(user)
                };

                userStatuses.Add(currentUserStatus);
            }

            return userStatuses.OrderByDescending(x => x.IsOnline).ThenBy(x => x.Username);
        }

        public async Task<IEnumerable<UserRole>> GetAllRolesAsync()
        {
            var allRoles = await _roleManager.Roles.Select(x => new { x.Id, x.Name }).ToListAsync();
            var userRoles = new List<UserRole>();

            foreach (var role in allRoles)
            {
                var currentRole = new UserRole
                {
                    RoleId = role.Id,
                    RoleName = role.Name
                };
                userRoles.Add(currentRole);
            }
            return userRoles;
        }

        public bool Logout(string token)
        {
            try
            {
                var storedToken = _tokenDbContext.Tokens.Single(x => x.UserToken == token);
                _tokenDbContext.Remove(storedToken);
                _tokenDbContext.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

       

        /// <summary>
        ///     It proof if the token is valid or not
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private bool IsTokenValid(string token)
        {
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(_appSetting.JwtSecret.ToByteArray()),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero,
                ValidateLifetime = true,
               // RoleClaimType = "roles"
            };
            var tokenHandler = new JwtSecurityTokenHandler();

            try
            {
                // ReSharper disable once UnusedVariable
                tokenHandler.ValidateToken(token, validationParameters, out var securityToken);
            }
            // Catch specific exception
            catch (Exception)
            {
                return false;
            }
            return true;
        }

     
    }
}