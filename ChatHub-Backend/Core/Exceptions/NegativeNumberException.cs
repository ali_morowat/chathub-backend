﻿using System;

namespace ChatHub_Backend.Core.Exceptions
{
    public class NegativeNumberException : Exception
    {
        public NegativeNumberException() : base("Positive number expected.")
        {
        }

        public NegativeNumberException(int number) : base($"{number} is negative. Positive number expected.")
        {
        }
    }
}