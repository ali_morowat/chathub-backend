﻿using System;
using System.Net.Mail;
using System.Text;

namespace ChatHub_Backend.Core.ExtensionMethods
{
    public static class StringExtensions
    {
        public static byte[] ToByteArray(this string sourceString, Encoding encoding)
        {
            encoding = encoding ?? Encoding.UTF8;

            return encoding.GetBytes(sourceString);
        }

        public static byte[] ToByteArray(this string sourceString)
        {
            return sourceString.ToByteArray(Encoding.UTF8);
        }

        public static string DeleteLast(this string sourceString)
        {
            return sourceString.Remove(sourceString.Length - 1);
        }

        /// <summary>
        ///     Returns true if ths provided string represents an email, otherwise false
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static bool IsEmail(this string sourceString)
        {
            try
            {
                var email = new MailAddress(sourceString);
                return email.Address == sourceString;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}