﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ChatHub_Backend.Core.ExtensionMethods
{
    /// <summary>
    ///     Extension methods for IEnumerable
    /// </summary>
    public static class EnumerableExtensions
    {
        /// <summary>
        ///     Returns 'true' if the IEnumerable is null or has 0 elements
        ///     Otherwise 'false'
        /// </summary>
        /// <typeparam name="T">Type of the list</typeparam>
        /// <param name="list">List of object of type T</param>
        /// <returns></returns>
        public static bool IsEmpty<T>(this IEnumerable<T> list)
        {
            return list == null || !list.Any();
        }

        public static IEnumerable<List<T>> Partition<T>(this IQueryable<T> source)
        {
            var partitionSize = source.Count() / Environment.ProcessorCount;

            for (var i = 0; i < Math.Ceiling(source.Count() / (double) partitionSize); i++)
                yield return new List<T>(source.Skip(partitionSize * i).Take(partitionSize));
        }

        public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
        {
            return items.GroupBy(property).Select(x => x.First());
        }

        /// <summary>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceList"></param>
        /// <param name="chunkSize"></param>
        /// <returns></returns>
        public static List<List<T>> Partition<T>(this IEnumerable<T> sourceList, int chunkSize)
        {
            return sourceList
                .Select((value, index) => new {Index = index, Value = value})
                .GroupBy(group => group.Index / chunkSize)
                .Select(group => group.Select(grp => grp.Value).ToList())
                .ToList();
        }

        /// <summary>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sourceList"></param>
        /// <returns></returns>
        public static List<List<T>> Partition<T>(this IEnumerable<T> sourceList)
        {
            // ReSharper disable once PossibleMultipleEnumeration
            var chunkSize = (int) Math.Floor(Math.Sqrt(sourceList.Count()));

            return Partition(sourceList, chunkSize);
        }
    }
}