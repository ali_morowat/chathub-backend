using FluentAssertions;
using Microsoft.OpenApi.Expressions;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using UserManagement.Models;
using Xunit;

namespace UserManagement.IntegrationTest
{
    public class UmApiIntegrationTest
    {
        [Fact]
        public async Task TestGetAll()
        {
            using (var client = new TestClientProvider().Client)
            {
                //act
                var request = "/api/AspNetUsers/Users";
                var responce = await client.GetAsync(request);
                // assert
                responce.EnsureSuccessStatusCode();
                responce.StatusCode.Should().Be(HttpStatusCode.OK);
            }
       }

        [Fact]
        public async Task Testpost()
        {
            using (var client = new TestClientProvider().Client)
            {
                var request = new
                {
                    // Arrange
                    Url = "/api/v1/Warehouse/StockItem",
                    Body = new
                    {
                        id = "574edef9 - 3896 - 4ff3 - ba9b - 43c5be778eda",
	                    userName = "Anan",
	                    normalizedUserName = "ANAN",
	                    email = "Anan123 @gmail.com",
	                    normalizedEmail = "ANAN123@GMAIL.COM",
	                    emailConfirmed = false,
	                    passwordHash = "AQAAAAEAACcQAAAAEIHJhlnjTc4QPqOgXNiqGmXikX2uPmr4MT + maMdiasxBXEDFDAT0ZhNxYZt5HN4pKQ == ",

                        securityStamp = "36FQK7YDJHPOMGPPXEILSZE4UKW64TOP",
	                    concurrencyStamp = "415c6ae7 - 659a - 4742 - b703 - 8bb58d515c0b",
	                    phoneNumber = "null",
	                    phoneNumberConfirmed = false,
	                    twoFactorEnabled = false,
	                    lockoutEnd = "null",
	                    lockoutEnabled = true,
	                    accessFailedCount = 0,
	                    discriminator = "ApplicationUser",
	                    fullName = "Anna M",
                        isUserLogged = false, 
	                    aspNetUserClaims = "[]",
	                    aspNetUserLogins ="[]",
	                    aspNetUserRoles ="[]",
	                    aspNetUserTokens = "[]"
                    }
                };
                // Act
                var responce = await client.PostAsync(request.Url,
                         new StringContent(JsonConvert.SerializeObject(request.Body), Encoding.UTF8, "application/json"));
                // Assert
                responce.EnsureSuccessStatusCode();
                responce.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }

        [Fact]
        public async Task TestGetById()
        {
            using (var client = new TestClientProvider().Client)
            {
                //Act
                var request = "/api/AspNetUsers/574edef9-3896-4ff3-ba9b-43c5be778eda";
                var responce = await client.GetAsync(request) ;
                // Assert
                responce.EnsureSuccessStatusCode();
                responce.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }


        [Fact]
        public async Task TestDeleteById()
        {
            using (var client = new TestClientProvider().Client)
            {
                //Act
                var request = "/api/AspNetUsers/662766a8-9d11-4fe7-944d-c447910d01c0";
                var responce = await client.DeleteAsync(request);
                //Assert
                responce.EnsureSuccessStatusCode();
                responce.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }
    }
}
